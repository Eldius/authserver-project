package net.eldiosantos.authserver.model.auth;

import lombok.Builder;
import lombok.Data;
import net.eldiosantos.authserver.model.auth.profile.UserProfile;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by eldio.junior on 08/05/2015.
 */
@Entity
@Data
@Builder
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String user;

    @Lob
    private String pass;
    private String salt;

    @OneToOne(orphanRemoval = true, cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn()
    private UserProfile profile = UserProfile.builder().build();

    public Credentials getCredentials() {
        return Credentials.builder()
                .pass(getPass())
                .user(getUser())
                .salt(getSalt())
                .build();
    }

    public User setCredentials(final Credentials credentials) {
        setPass(credentials.getPass());
        setUser(credentials.getUser());
        setSalt(credentials.getSalt());
        return this;
    }
}
