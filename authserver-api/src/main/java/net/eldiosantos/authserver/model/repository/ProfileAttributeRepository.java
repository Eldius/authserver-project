package net.eldiosantos.authserver.model.repository;


import net.eldiosantos.authserver.model.auth.profile.ProfileAttribute;
import net.eldiosantos.authserver.model.auth.profile.ProfileAttributeId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileAttributeRepository extends JpaRepository<ProfileAttribute, ProfileAttributeId> {

}
