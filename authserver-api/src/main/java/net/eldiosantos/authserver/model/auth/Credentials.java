package net.eldiosantos.authserver.model.auth;

import lombok.Builder;
import lombok.Data;

/**
 * Created by Eldius on 16/05/2015.
 */
@Data
@Builder
public class Credentials {
    private String user;
    private String pass;
    private String salt;

}
