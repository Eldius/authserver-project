package net.eldiosantos.authserver.model.auth.profile;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eldio.junior on 22/05/2015.
 */
@Entity
@Data
@Builder
public class UserProfile {

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(mappedBy = "userProfile")
    private List<ProfileAttribute>attributes = new ArrayList<>();

}
