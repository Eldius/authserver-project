package net.eldiosantos.authserver.model.auth;

import lombok.Builder;
import lombok.Data;
import org.joda.time.DateTime;
import org.joda.time.Period;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Eldius on 16/05/2015.
 */
@Entity
@Data
@Builder
public class UserSessionAuth implements Serializable {

    public static enum ExpirationType {
        SHORT_TERM(Period.minutes(15))
        , LONG_TERM(Period.years(1));

        private final Period period;

        ExpirationType(Period period) {
            this.period = period;
        }

        public Period period() {
            return period;
        }
    }

    @Id
    private String token;

    @ManyToOne
    private User user;

    @Temporal(TemporalType.TIMESTAMP)
    private Date validUntil;

    private ExpirationType expirationTime;

    public void setValidUntil(DateTime validUntil) {
        this.validUntil = validUntil.toDate();
    }

    public DateTime getValidUntil() {
        return new DateTime(validUntil);
    }

    public UserSessionAuth renew() {
        this.setValidUntil(DateTime.now().plus(getExpirationTime().period()));
        return this;
    }

    public Boolean isValid() {
        return this.getValidUntil().isAfter(DateTime.now());
    }

    public UserSessionAuth invalidate() {
        this.setValidUntil(new DateTime(System.currentTimeMillis() - 1));
        return this;
    }
}
