package net.eldiosantos.authserver.model.repository;

import net.eldiosantos.authserver.model.auth.UserSessionAuth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserSessionAuthRepository extends JpaRepository<UserSessionAuth, String> {

}
