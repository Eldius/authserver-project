package net.eldiosantos.authserver.model.repository;


import net.eldiosantos.authserver.model.auth.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    public User findByUser(String user);
}
