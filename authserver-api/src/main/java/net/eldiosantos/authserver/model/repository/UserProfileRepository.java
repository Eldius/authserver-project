package net.eldiosantos.authserver.model.repository;


import net.eldiosantos.authserver.model.auth.profile.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {

}
