package net.eldiosantos.authserver.service.builder;

import net.eldiosantos.authserver.service.builder.impl.CredentialsBuilderImpl;

import javax.inject.Inject;

/**
 * Created by Eldius on 16/05/2015.
 */
public class CredentialsBuilder {

    @Inject
    private CredentialsBuilderImpl builder;

    public UserCredentialsBuilder start() {
        return builder.start();
    }
}
