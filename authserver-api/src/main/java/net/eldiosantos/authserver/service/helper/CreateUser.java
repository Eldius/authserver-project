package net.eldiosantos.authserver.service.helper;

import net.eldiosantos.authserver.model.auth.Credentials;
import net.eldiosantos.authserver.model.auth.User;
import net.eldiosantos.authserver.model.repository.UserRepository;
import net.eldiosantos.authserver.resource.CredentialsVO;
import net.eldiosantos.authserver.service.builder.CredentialsBuilder;

import javax.inject.Inject;

/**
 * Created by Eldius on 11/07/2015.
 */
public class CreateUser {

    @Inject
    private CredentialsBuilder credentialsBuilder;

    @Inject
    private UserRepository userRepository;

    public User create(final CredentialsVO vo) throws Exception {
        final Credentials credentials = credentialsBuilder.start()
                .user(vo.getUser())
                .generateSalt()
                .pass(vo.getPass());

        final User user = User.builder().build().setCredentials(credentials);
        return userRepository.save(user);
    }
}
