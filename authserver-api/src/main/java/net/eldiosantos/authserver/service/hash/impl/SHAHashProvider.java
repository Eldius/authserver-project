package net.eldiosantos.authserver.service.hash.impl;

import net.eldiosantos.authserver.service.HashProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by esjunior on 04/02/2016.
 */
public class SHAHashProvider extends HashProvider {

    @Override
    public byte[] hash(final String phrase) throws Exception {
        final MessageDigest hasher = getMessageDigest();
        return hasher.digest(phrase.getBytes("UTF-8"));
    }

    public byte[] hash(final String phrase, final String salt) throws Exception {
        final MessageDigest hasher = getMessageDigest();
        hasher.reset();
        hasher.update(salt.getBytes());
        return hasher.digest(phrase.getBytes("UTF-8"));
    }

    private MessageDigest getMessageDigest() throws NoSuchAlgorithmException {
        return super.getMessageDigest("SHA-256");
    }
}
