package net.eldiosantos.authserver.service.builder.impl;

import net.eldiosantos.authserver.model.auth.Credentials;
import net.eldiosantos.authserver.service.builder.PassCredentialsBuilder;
import net.eldiosantos.authserver.service.builder.SaltCredentialsBuilder;
import net.eldiosantos.authserver.service.builder.UserCredentialsBuilder;
import net.eldiosantos.authserver.service.hash.HASHProvider;
import org.apache.commons.codec.binary.Hex;

import javax.inject.Inject;
import java.security.SecureRandom;

/**
 * Created by Eldius on 16/05/2015.
 */
public class CredentialsBuilderImpl implements PassCredentialsBuilder, SaltCredentialsBuilder, UserCredentialsBuilder {

    @Inject
    private HASHProvider hashProvider;

    private Credentials credentials;

    public CredentialsBuilderImpl start() {
        this.credentials = Credentials.builder().build();
        return this;
    }

    @Override
    public Credentials pass(String pass) throws Exception {
        if(pass == null || pass.isEmpty()) {
            throw new IllegalArgumentException("The password could not be empty");
        }
        return Credentials.builder().pass(hashProvider.stringHash(pass, credentials.getSalt())).build();
    }

    @Override
    public PassCredentialsBuilder salt(final String salt) {
        credentials.setSalt(salt);
        return this;
    }

    @Override
    public PassCredentialsBuilder generateSalt() throws Exception {
        byte[] binarySalt = new byte[16];
        SecureRandom.getInstance("SHA1PRNG").nextBytes(binarySalt);

        final String salt = Hex.encodeHexString(binarySalt);
        credentials.setSalt(salt);
        return this;
    }

    @Override
    public SaltCredentialsBuilder user(final String user) {
        if(user == null || user.isEmpty()) {
            throw new IllegalArgumentException("The username could not be empty");
        }
        credentials.setUser(user);
        return this;
    }
}
