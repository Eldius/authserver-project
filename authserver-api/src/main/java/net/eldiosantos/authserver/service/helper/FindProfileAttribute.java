package net.eldiosantos.authserver.service.helper;

import net.eldiosantos.authserver.model.auth.User;
import net.eldiosantos.authserver.model.auth.profile.ProfileAttribute;
import net.eldiosantos.authserver.model.auth.profile.UserProfile;
import net.eldiosantos.authserver.model.repository.UserRepository;

import javax.inject.Inject;
import java.util.function.Predicate;

/**
 * Created by esjunior on 04/08/2015.
 */
public class FindProfileAttribute {

    @Inject
    private UserRepository userRepository;

    public ProfileAttribute find(final User user, final String attributeName) {
        final UserProfile profile = userRepository.findOne(user.getId()).getProfile();

        return
            profile.getAttributes().stream()
                .filter(patt->patt.getAttributeName().equals(attributeName))
                .findAny()
                .orElseGet(() -> {
                    ProfileAttribute patt = new ProfileAttribute(attributeName, null);
                    profile.getAttributes().add(patt);
                    return patt;
                });
    }
}
