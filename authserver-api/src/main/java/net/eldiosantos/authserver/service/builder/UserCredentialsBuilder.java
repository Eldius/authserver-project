package net.eldiosantos.authserver.service.builder;

/**
 * Created by Eldius on 16/05/2015.
 */
public interface UserCredentialsBuilder {
    public SaltCredentialsBuilder user(String user);
}
