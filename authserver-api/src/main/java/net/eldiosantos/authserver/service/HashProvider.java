package net.eldiosantos.authserver.service;

import org.apache.commons.codec.binary.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by esjunior on 04/02/2016.
 */
public abstract class HashProvider {
    public abstract byte[] hash(final String phrase) throws Exception;

    public abstract byte[] hash(final String phrase, final String salt) throws Exception;

    public String stringHash(final String phrase) throws Exception {
        return Hex.encodeHexString(hash(phrase));
    }

    public String stringHash(final String phrase, final String salt) throws Exception {
        return Hex.encodeHexString(hash(phrase, salt));
    }

    protected MessageDigest getMessageDigest(final String algorithm) throws NoSuchAlgorithmException {
        return MessageDigest.getInstance(algorithm);
    }
}
