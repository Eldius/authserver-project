package net.eldiosantos.authserver.service.helper;

import net.eldiosantos.authserver.model.auth.Credentials;
import net.eldiosantos.authserver.model.auth.User;
import net.eldiosantos.authserver.model.repository.UserRepository;
import net.eldiosantos.authserver.resource.CredentialsVO;
import net.eldiosantos.authserver.service.builder.CredentialsBuilder;

import javax.inject.Inject;

/**
 * Created by esjunior on 23/07/2015.
 */
public class UpdateUser {

    @Inject
    private UserRepository userRepository;

    @Inject
    private CredentialsBuilder credentialsBuilder;

    /**
     * Just for CDI
     */
    @Deprecated
    public UpdateUser() {
    }

    public UpdateUser(UserRepository userRepository, CredentialsBuilder credentialsBuilder) {
        this.userRepository = userRepository;
        this.credentialsBuilder = credentialsBuilder;
    }

    public User update(final CredentialsVO vo) throws Exception {
        final User user = userRepository.findByUser(vo.getUser());
        final Credentials credentials = credentialsBuilder.start()
                .user(user.getUser())
                .generateSalt()
                .pass(vo.getPass());
        user.setCredentials(credentials);

        userRepository.save(user);

        return user;
    }
}
