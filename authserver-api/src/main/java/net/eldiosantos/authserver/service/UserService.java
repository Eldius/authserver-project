package net.eldiosantos.authserver.service;

import net.eldiosantos.authserver.model.auth.Credentials;
import net.eldiosantos.authserver.model.auth.User;
import net.eldiosantos.authserver.model.repository.UserRepository;
import net.eldiosantos.authserver.resource.CredentialsVO;
import net.eldiosantos.authserver.resource.UserVO;
import net.eldiosantos.authserver.service.builder.CredentialsBuilder;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.inject.Inject;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Eldius on 09/05/2015.
 */
public class UserService {

    @Inject
    private UserRepository userRepository;

    @Inject
    private CredentialsBuilder credentialsBuilder;

    @Inject
    private ModelMapper modelMapper;

    /**
     * Just for CDI.
     */
    @Deprecated
    public UserService() {
    }

    public UserService(UserRepository userRepository, CredentialsBuilder credentialsBuilder, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.credentialsBuilder = credentialsBuilder;
        this.modelMapper = modelMapper;
    }

    public UserVO create(final CredentialsVO cred) throws Exception {
        final Credentials credentials = credentialsBuilder.start()
                .user(cred.getUser())
                .generateSalt()
                .pass(cred.getPass());
        final User user = User.builder().build()
                .setCredentials(credentials);
        userRepository.save(user);

        return modelMapper.map(user, UserVO.class);
    }

    public List<UserVO> list() {
        final Type targetListType = new TypeToken<List<UserVO>>() {
        }.getType();
        return modelMapper.map(userRepository.findAll(), targetListType);
    }

    public UserVO get(final Long id) {
        return modelMapper.map(userRepository.findOne(id), UserVO.class);
    }

    public void delete(final Long id) {
        userRepository.delete(userRepository.findOne(id));
    }

}
