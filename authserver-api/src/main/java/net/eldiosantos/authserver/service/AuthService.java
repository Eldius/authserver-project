package net.eldiosantos.authserver.service;

import net.eldiosantos.authserver.model.auth.Credentials;
import net.eldiosantos.authserver.model.auth.User;
import net.eldiosantos.authserver.model.auth.UserSessionAuth;
import net.eldiosantos.authserver.model.repository.UserRepository;
import net.eldiosantos.authserver.model.repository.UserSessionAuthRepository;
import net.eldiosantos.authserver.resource.CredentialsVO;
import net.eldiosantos.authserver.service.builder.CredentialsBuilder;
import net.eldiosantos.authserver.service.support.TokenHeaderExtractor;
import net.eldiosantos.authserver.service.token.TokenGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Created by Eldius on 16/05/2015.
 */
public class AuthService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    private TokenGenerator tokenGenerator;

    @Inject
    private UserSessionAuthRepository sessionAuthRepository;

    @Inject
    private CredentialsBuilder builder;

    @Inject
    private UserRepository userRepository;

    @Inject
    private TokenHeaderExtractor tokenHeaderExtractor;

    /**
     * Just for CDI.
     */
    @Deprecated
    public AuthService() {
    }

    public AuthService(TokenGenerator tokenGenerator, UserSessionAuthRepository sessionAuthRepository, CredentialsBuilder builder, UserRepository userRepository, TokenHeaderExtractor tokenHeaderExtractor) {
        this.tokenGenerator = tokenGenerator;
        this.sessionAuthRepository = sessionAuthRepository;
        this.builder = builder;
        this.userRepository = userRepository;
        this.tokenHeaderExtractor = tokenHeaderExtractor;
    }

    public UserSessionAuth login(final CredentialsVO loginData) throws Exception {
        try {
            final User user = userRepository.findByUser(loginData.getUser());
            final Credentials credentials = builder.start()
                    .user(loginData.getUser())
                    .salt(user.getSalt())
                    .pass(loginData.getPass());

            if (user.getCredentials().equals(credentials)) {
                final UserSessionAuth auth = tokenGenerator.generate(user);
                sessionAuthRepository.save(auth);
                return auth;
            }
        }catch (Exception e) {
            logger.warn("Error while trying to authenticate user", e);
        }
        return null;
    }

    public void logout() {
        sessionAuthRepository.save(sessionAuthRepository.findOne(tokenHeaderExtractor.extract()).invalidate());
    }
}
