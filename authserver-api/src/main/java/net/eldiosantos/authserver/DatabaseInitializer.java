package net.eldiosantos.authserver;

import net.eldiosantos.authserver.model.auth.User;
import net.eldiosantos.authserver.model.repository.UserRepository;
import net.eldiosantos.authserver.service.builder.CredentialsBuilder;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import javax.inject.Inject;

@Configuration
public class DatabaseInitializer implements CommandLineRunner{

	@Inject
	private UserRepository userRepository;

    @Inject
    private CredentialsBuilder credentialsBuilder;

	@Override
	public void run(String... arg0) throws Exception {

		final User u1 = User.builder().build()
            .setCredentials(
                credentialsBuilder.start()
                    .user("user1")
                    .generateSalt()
                    .pass("pass1")
            );

		final User u2 = User.builder().build()
            .setCredentials(
                credentialsBuilder.start()
                .user("user2")
                .generateSalt()
                .pass("pass2")

        );

		final User u3 = User.builder().build()
            .setCredentials(
                credentialsBuilder.start()
                    .user("user3")
                    .generateSalt()
                    .pass("pass3")
            );

		userRepository.save(u1);
		userRepository.save(u2);
		userRepository.save(u3);

		userRepository.flush();

		userRepository.findAll()
		    .forEach(p -> System.out.println(p.getId()));
	}
}
