package net.eldiosantos.authserver.resource;

import lombok.Builder;
import lombok.Data;
import net.eldiosantos.authserver.model.auth.User;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

/**
 * Created by esjunior on 21/01/2016.
 */
@ApiObject
@Data
@Builder
public class UserResponse {

    @ApiObjectField(description = "The User's identifier")
    private Long id;

    @ApiObjectField(description = "The User's login")
    private String user;

}
