package net.eldiosantos.authserver.resource;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Eldius on 16/05/2015.
 */
@Data
@Builder
public class LoginResultVO implements Serializable {
    private Boolean success;
    private String token;
    private String message;

    public Boolean getSuccess() {
        return success;
    }
}
