package net.eldiosantos.authserver.resource;

import lombok.Data;
import org.jsondoc.core.annotation.ApiObject;

/**
 * Created by esjunior on 21/01/2016.
 */
@ApiObject
@Data
public class UserCredentials {
    private String user;
    private String pass;
}
