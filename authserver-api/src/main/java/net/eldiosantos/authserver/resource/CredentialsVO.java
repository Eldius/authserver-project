package net.eldiosantos.authserver.resource;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Eldius on 09/05/2015.
 */
@Data
@Builder
public class CredentialsVO implements Serializable {
    private String user;
    private String pass;
}
