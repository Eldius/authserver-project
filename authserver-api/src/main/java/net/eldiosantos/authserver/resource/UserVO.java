package net.eldiosantos.authserver.resource;

import lombok.Builder;
import lombok.Data;
import net.eldiosantos.authserver.model.auth.profile.UserProfile;

import java.io.Serializable;

/**
 * Created by Eldius on 09/05/2015.
 */
@Data
@Builder
public class UserVO implements Serializable {
    private Long id;

    private String user;

    private UserProfile role;

}
