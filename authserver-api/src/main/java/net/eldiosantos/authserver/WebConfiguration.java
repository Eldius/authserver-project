package net.eldiosantos.authserver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.h2.server.web.WebServlet;
import org.springframework.boot.context.embedded.ServletRegistrationBean;

@Configuration
public class WebConfiguration {

	@Bean
	ServletRegistrationBean h2ServletRegistration(){
		ServletRegistrationBean registrationBean = new ServletRegistrationBean( new WebServlet() );
		registrationBean.addUrlMappings("/h2-console/*");
		return registrationBean;
	}
    
}

