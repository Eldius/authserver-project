package net.eldiosantos.authserver.controller;

import net.eldiosantos.authserver.model.auth.User;
import net.eldiosantos.authserver.model.repository.UserRepository;
import net.eldiosantos.authserver.resource.UserCredentials;
import net.eldiosantos.authserver.resource.UserResponse;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiBodyObject;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by esjunior on 21/01/2016.
 */
@RestController
@RequestMapping(value = "/auth", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
@Api( description = "The authorization controller", name = " Auth services")
public class AuthController {

    private UserRepository userRepository;

    @Inject
    public AuthController(UserRepository useruserRepository) {
        this.userRepository = useruserRepository;
    }

    @ApiMethod(description = "Get an specific product")
    @RequestMapping( value = "/", method = POST)
    public @ApiResponseObject UserResponse findOne(@ApiBodyObject UserCredentials credentials){
        final User user = userRepository.findByUser(credentials.getUser());

        return UserResponse.builder().user(user.getUser()).id(user.getId()).build();
    }


}
