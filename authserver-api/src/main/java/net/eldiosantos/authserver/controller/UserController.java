package net.eldiosantos.authserver.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.util.List;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import net.eldiosantos.authserver.model.auth.User;
import net.eldiosantos.authserver.model.repository.UserRepository;
import net.eldiosantos.authserver.resource.UserCredentials;
import net.eldiosantos.authserver.resource.UserResponse;
import net.eldiosantos.authserver.service.builder.CredentialsBuilder;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping(value = "/userRepository", produces = APPLICATION_JSON_VALUE)
@Api( description = "The user controller", name = " User services")
@Slf4j
public class UserController {

    @Inject
	private UserRepository userRepository;

    @Inject
    private CredentialsBuilder credentialsBuilder;

    @Deprecated
    public UserController() {
    }

    public UserController(UserRepository userRepository, CredentialsBuilder credentialsBuilder) {
		this.userRepository = userRepository;
        this.credentialsBuilder = credentialsBuilder;
	}

	@ApiMethod(description = "Get an specific user")
	@RequestMapping( value = "/{id}", method = GET)
	public @ApiResponseObject UserResponse findOne(@ApiPathParam(name = "id") @PathVariable Long id){
        final User user = userRepository.findOne(id);
        return UserResponse.builder().id(user.getId()).user(user.getUser()).build();
    }

	@ApiMethod(description = "List all users")
	@RequestMapping( value = "/", method = GET)
	public @ApiResponseObject List<UserResponse> findAll(){
		return userRepository.findAll()
				.stream()
				.map(u->UserResponse.builder().id(u.getId()).user(u.getUser()).build())
                .collect(Collectors.toList());
	}

    @ApiMethod(description = "Save a user")
    @RequestMapping( value = "/", method = POST)
    public @ApiResponseObject UserResponse save(@RequestBody UserCredentials credentials) {
        try {
            final User user = userRepository.save(
                User.builder().build()
                    .setCredentials(
                        credentialsBuilder
                            .start()
                            .user(credentials.getUser())
                            .generateSalt()
                            .pass(credentials.getPass())
                    )
            );
            return UserResponse.builder().user(user.getUser()).id(user.getId()).build();
        }catch (Exception e) {
            log.error(String.format("Error trying to save a new user (%s)", credentials.getUser()), e);
            return null;
        }
    }
}
