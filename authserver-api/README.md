
## rodar aplicação ##
* mvn spring-boot:run
* mvn spring-boot:run -Dserver.port=<port number>
* mvn clean package && java -jar target/smart-price-api.jar

## para acessar a documentacao da api ##
* acesse [http://localhost:8080/jsondoc-ui.html] (http://localhost:8080/jsondoc-ui.html)
* na janela de busca digite: [http://localhost:8080/jsondoc](http://localhost:8080/jsondoc)

## para acessar o h2 ##
* acesse [http://localhost:8080/h2-console/] (http://localhost:8080/h2-console/)
* usuario sa sem senha

## configurar porta da aplicação: ##
* -Dserver.port=9090
